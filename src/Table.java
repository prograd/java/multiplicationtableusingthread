public class Table extends Thread{
    private Integer tableNumber;
    Thread thread;
    Table(Integer tableNumber){
        this.tableNumber=tableNumber;
        thread=new Thread(this);
        thread.start();
    }

    @Override
    public void run() {

        for (int iterate = 1; iterate <=  10; iterate++) {
            System.out.println(tableNumber+"x"+iterate+"= "+tableNumber*iterate);
        }

    }

    public static void main(String[] args) {
        Table table=new Table(2);
    }
}
