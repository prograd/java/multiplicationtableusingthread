public class SampleUsingRunnable implements Runnable{
    private Integer tableNumber;
    Thread thread;
    SampleUsingRunnable(Integer tableNumber,String threadName){
        this.tableNumber=tableNumber;
        thread=new Thread(this,threadName);
        thread.start();
    }
    @Override
    public void run() {
        for (int iterate = 1; iterate <=  10; iterate++) {
            System.out.println(tableNumber+"x"+iterate+"= "+tableNumber*iterate);
        }
    }
    public static void main(String[] args) {
        SampleUsingRunnable sampleUsingRunnable=new SampleUsingRunnable(3,"table Of three");

    }
}
